﻿namespace SearchForPicturesWithPictures
{
    partial class FrmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DataGridViewCellStyle dataGridViewCellStyle1 = new DataGridViewCellStyle();
            tableLayoutPanel1 = new TableLayoutPanel();
            tableLayoutPanel3 = new TableLayoutPanel();
            dgvInfo = new DataGridView();
            Column3 = new DataGridViewTextBoxColumn();
            Column1 = new DataGridViewLinkColumn();
            Column5 = new DataGridViewTextBoxColumn();
            rtxtMsg = new RichTextBox();
            tableLayoutPanel2 = new TableLayoutPanel();
            groupBox2 = new GroupBox();
            pbSub = new PictureBox();
            groupBox1 = new GroupBox();
            pbTarget = new PictureBox();
            groupBox3 = new GroupBox();
            waitingControl1 = new View.WaitingControl();
            lblMessage = new Label();
            lblCount = new Label();
            btnLoadRootPath = new Button();
            txtRootPath = new TextBox();
            label2 = new Label();
            numericUpDown1 = new NumericUpDown();
            label1 = new Label();
            btnStop = new Button();
            btnSearch = new Button();
            tableLayoutPanel1.SuspendLayout();
            tableLayoutPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)dgvInfo).BeginInit();
            tableLayoutPanel2.SuspendLayout();
            groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)pbSub).BeginInit();
            groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)pbTarget).BeginInit();
            groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)numericUpDown1).BeginInit();
            SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            tableLayoutPanel1.ColumnCount = 2;
            tableLayoutPanel1.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 62.4999962F));
            tableLayoutPanel1.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 37.5000038F));
            tableLayoutPanel1.Controls.Add(tableLayoutPanel3, 0, 0);
            tableLayoutPanel1.Controls.Add(tableLayoutPanel2, 1, 0);
            tableLayoutPanel1.Dock = DockStyle.Fill;
            tableLayoutPanel1.Location = new Point(0, 0);
            tableLayoutPanel1.Name = "tableLayoutPanel1";
            tableLayoutPanel1.RowCount = 1;
            tableLayoutPanel1.RowStyles.Add(new RowStyle(SizeType.Percent, 100F));
            tableLayoutPanel1.Size = new Size(906, 601);
            tableLayoutPanel1.TabIndex = 0;
            // 
            // tableLayoutPanel3
            // 
            tableLayoutPanel3.ColumnCount = 1;
            tableLayoutPanel3.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
            tableLayoutPanel3.Controls.Add(dgvInfo, 0, 0);
            tableLayoutPanel3.Controls.Add(rtxtMsg, 0, 1);
            tableLayoutPanel3.Dock = DockStyle.Fill;
            tableLayoutPanel3.Location = new Point(3, 3);
            tableLayoutPanel3.Name = "tableLayoutPanel3";
            tableLayoutPanel3.RowCount = 2;
            tableLayoutPanel3.RowStyles.Add(new RowStyle(SizeType.Percent, 100F));
            tableLayoutPanel3.RowStyles.Add(new RowStyle(SizeType.Absolute, 150F));
            tableLayoutPanel3.Size = new Size(560, 595);
            tableLayoutPanel3.TabIndex = 1;
            // 
            // dgvInfo
            // 
            dgvInfo.AllowUserToAddRows = false;
            dgvInfo.AllowUserToDeleteRows = false;
            dgvInfo.AllowUserToResizeRows = false;
            dgvInfo.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            dataGridViewCellStyle1.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = SystemColors.Control;
            dataGridViewCellStyle1.Font = new Font("Microsoft YaHei UI", 9F, FontStyle.Regular, GraphicsUnit.Point);
            dataGridViewCellStyle1.ForeColor = SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = DataGridViewTriState.True;
            dgvInfo.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            dgvInfo.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dgvInfo.Columns.AddRange(new DataGridViewColumn[] { Column3, Column1, Column5 });
            dgvInfo.Dock = DockStyle.Fill;
            dgvInfo.Location = new Point(3, 3);
            dgvInfo.Name = "dgvInfo";
            dgvInfo.ReadOnly = true;
            dgvInfo.RowHeadersVisible = false;
            dgvInfo.RowTemplate.Height = 25;
            dgvInfo.Size = new Size(554, 439);
            dgvInfo.TabIndex = 1;
            dgvInfo.CellClick += dgvInfo_CellClick;
            // 
            // Column3
            // 
            Column3.DataPropertyName = "Index";
            Column3.FillWeight = 30F;
            Column3.HeaderText = "序号";
            Column3.Name = "Column3";
            Column3.ReadOnly = true;
            // 
            // Column1
            // 
            Column1.DataPropertyName = "FilePath";
            Column1.HeaderText = "图像路径";
            Column1.Name = "Column1";
            Column1.ReadOnly = true;
            Column1.Resizable = DataGridViewTriState.True;
            Column1.SortMode = DataGridViewColumnSortMode.Automatic;
            // 
            // Column5
            // 
            Column5.DataPropertyName = "SimilarityScore";
            Column5.FillWeight = 30F;
            Column5.HeaderText = "相似度";
            Column5.Name = "Column5";
            Column5.ReadOnly = true;
            // 
            // rtxtMsg
            // 
            rtxtMsg.Dock = DockStyle.Fill;
            rtxtMsg.Location = new Point(3, 448);
            rtxtMsg.Name = "rtxtMsg";
            rtxtMsg.ReadOnly = true;
            rtxtMsg.Size = new Size(554, 144);
            rtxtMsg.TabIndex = 2;
            rtxtMsg.Text = "";
            rtxtMsg.TextChanged += rtxtMsg_TextChanged;
            // 
            // tableLayoutPanel2
            // 
            tableLayoutPanel2.ColumnCount = 1;
            tableLayoutPanel2.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
            tableLayoutPanel2.Controls.Add(groupBox2, 0, 2);
            tableLayoutPanel2.Controls.Add(groupBox1, 0, 0);
            tableLayoutPanel2.Controls.Add(groupBox3, 0, 1);
            tableLayoutPanel2.Dock = DockStyle.Fill;
            tableLayoutPanel2.Location = new Point(569, 3);
            tableLayoutPanel2.Name = "tableLayoutPanel2";
            tableLayoutPanel2.RowCount = 3;
            tableLayoutPanel2.RowStyles.Add(new RowStyle(SizeType.Percent, 50F));
            tableLayoutPanel2.RowStyles.Add(new RowStyle(SizeType.Absolute, 100F));
            tableLayoutPanel2.RowStyles.Add(new RowStyle(SizeType.Percent, 50F));
            tableLayoutPanel2.Size = new Size(334, 595);
            tableLayoutPanel2.TabIndex = 1;
            // 
            // groupBox2
            // 
            groupBox2.Controls.Add(pbSub);
            groupBox2.Dock = DockStyle.Fill;
            groupBox2.Location = new Point(3, 350);
            groupBox2.Name = "groupBox2";
            groupBox2.Size = new Size(328, 242);
            groupBox2.TabIndex = 1;
            groupBox2.TabStop = false;
            groupBox2.Text = "查看图像";
            // 
            // pbSub
            // 
            pbSub.Dock = DockStyle.Fill;
            pbSub.Location = new Point(3, 19);
            pbSub.Name = "pbSub";
            pbSub.Size = new Size(322, 220);
            pbSub.SizeMode = PictureBoxSizeMode.Zoom;
            pbSub.TabIndex = 0;
            pbSub.TabStop = false;
            // 
            // groupBox1
            // 
            groupBox1.Controls.Add(pbTarget);
            groupBox1.Dock = DockStyle.Fill;
            groupBox1.Location = new Point(3, 3);
            groupBox1.Name = "groupBox1";
            groupBox1.Size = new Size(328, 241);
            groupBox1.TabIndex = 0;
            groupBox1.TabStop = false;
            groupBox1.Text = "选择搜索图像";
            // 
            // pbTarget
            // 
            pbTarget.Dock = DockStyle.Fill;
            pbTarget.Location = new Point(3, 19);
            pbTarget.Name = "pbTarget";
            pbTarget.Size = new Size(322, 219);
            pbTarget.SizeMode = PictureBoxSizeMode.Zoom;
            pbTarget.TabIndex = 0;
            pbTarget.TabStop = false;
            pbTarget.Click += pbTarget_Click;
            // 
            // groupBox3
            // 
            groupBox3.Controls.Add(waitingControl1);
            groupBox3.Controls.Add(lblMessage);
            groupBox3.Controls.Add(lblCount);
            groupBox3.Controls.Add(btnLoadRootPath);
            groupBox3.Controls.Add(txtRootPath);
            groupBox3.Controls.Add(label2);
            groupBox3.Controls.Add(numericUpDown1);
            groupBox3.Controls.Add(label1);
            groupBox3.Controls.Add(btnStop);
            groupBox3.Controls.Add(btnSearch);
            groupBox3.Dock = DockStyle.Fill;
            groupBox3.Location = new Point(3, 250);
            groupBox3.Name = "groupBox3";
            groupBox3.Size = new Size(328, 94);
            groupBox3.TabIndex = 2;
            groupBox3.TabStop = false;
            groupBox3.Text = "图像搜索";
            // 
            // waitingControl1
            // 
            waitingControl1.Activate = false;
            waitingControl1.EndCap = System.Drawing.Drawing2D.LineCap.Flat;
            waitingControl1.InnerRadius = 7.25F;
            waitingControl1.Location = new Point(275, 45);
            waitingControl1.Name = "waitingControl1";
            waitingControl1.NumberOfSpokes = 20;
            waitingControl1.Size = new Size(50, 39);
            waitingControl1.Speed = 100;
            waitingControl1.StartCap = System.Drawing.Drawing2D.LineCap.Flat;
            waitingControl1.TabIndex = 9;
            waitingControl1.Text = "请稍等";
            waitingControl1.Thickness = 3;
            // 
            // lblMessage
            // 
            lblMessage.AutoSize = true;
            lblMessage.Location = new Point(176, 74);
            lblMessage.Name = "lblMessage";
            lblMessage.Size = new Size(65, 17);
            lblMessage.TabIndex = 8;
            lblMessage.Text = "待搜索中...";
            // 
            // lblCount
            // 
            lblCount.AutoSize = true;
            lblCount.Location = new Point(6, 74);
            lblCount.Name = "lblCount";
            lblCount.Size = new Size(99, 17);
            lblCount.TabIndex = 7;
            lblCount.Text = "搜索文件总数：0";
            // 
            // btnLoadRootPath
            // 
            btnLoadRootPath.Location = new Point(225, 16);
            btnLoadRootPath.Name = "btnLoadRootPath";
            btnLoadRootPath.Size = new Size(66, 23);
            btnLoadRootPath.TabIndex = 6;
            btnLoadRootPath.Text = "加载路径";
            btnLoadRootPath.UseVisualStyleBackColor = true;
            btnLoadRootPath.Click += btnLoadRootPath_Click;
            // 
            // txtRootPath
            // 
            txtRootPath.Location = new Point(92, 16);
            txtRootPath.Name = "txtRootPath";
            txtRootPath.Size = new Size(127, 23);
            txtRootPath.TabIndex = 5;
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Location = new Point(6, 19);
            label2.Name = "label2";
            label2.Size = new Size(80, 17);
            label2.TabIndex = 4;
            label2.Text = "选择搜索路径";
            // 
            // numericUpDown1
            // 
            numericUpDown1.Location = new Point(44, 45);
            numericUpDown1.Maximum = new decimal(new int[] { 10000, 0, 0, 0 });
            numericUpDown1.Name = "numericUpDown1";
            numericUpDown1.Size = new Size(61, 23);
            numericUpDown1.TabIndex = 3;
            numericUpDown1.Value = new decimal(new int[] { 400, 0, 0, 0 });
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new Point(6, 47);
            label1.Name = "label1";
            label1.Size = new Size(32, 17);
            label1.TabIndex = 2;
            label1.Text = "阈值";
            // 
            // btnStop
            // 
            btnStop.Location = new Point(194, 45);
            btnStop.Name = "btnStop";
            btnStop.Size = new Size(75, 23);
            btnStop.TabIndex = 1;
            btnStop.Text = "停止搜索";
            btnStop.UseVisualStyleBackColor = true;
            btnStop.Click += btnStop_Click;
            // 
            // btnSearch
            // 
            btnSearch.Location = new Point(111, 45);
            btnSearch.Name = "btnSearch";
            btnSearch.Size = new Size(75, 23);
            btnSearch.TabIndex = 0;
            btnSearch.Text = "搜索";
            btnSearch.UseVisualStyleBackColor = true;
            btnSearch.Click += btnSearch_Click;
            // 
            // FrmMain
            // 
            AutoScaleDimensions = new SizeF(7F, 17F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(906, 601);
            Controls.Add(tableLayoutPanel1);
            Name = "FrmMain";
            ShowIcon = false;
            StartPosition = FormStartPosition.CenterScreen;
            Text = "以图搜图";
            FormClosing += FrmMain_FormClosing;
            tableLayoutPanel1.ResumeLayout(false);
            tableLayoutPanel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)dgvInfo).EndInit();
            tableLayoutPanel2.ResumeLayout(false);
            groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)pbSub).EndInit();
            groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)pbTarget).EndInit();
            groupBox3.ResumeLayout(false);
            groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)numericUpDown1).EndInit();
            ResumeLayout(false);
        }

        #endregion

        private TableLayoutPanel tableLayoutPanel1;
        private TableLayoutPanel tableLayoutPanel2;
        private GroupBox groupBox1;
        private PictureBox pbTarget;
        private GroupBox groupBox2;
        private PictureBox pbSub;
        private GroupBox groupBox3;
        private Button btnSearch;
        private Button btnStop;
        private Label label1;
        private NumericUpDown numericUpDown1;
        private Button btnLoadRootPath;
        private TextBox txtRootPath;
        private Label label2;
        private Label lblCount;
        private Label lblMessage;
        private TableLayoutPanel tableLayoutPanel3;
        private DataGridView dgvInfo;
        private DataGridViewTextBoxColumn Column3;
        private DataGridViewLinkColumn Column1;
        private DataGridViewTextBoxColumn Column5;
        private RichTextBox rtxtMsg;
        private View.WaitingControl waitingControl1;
    }
}