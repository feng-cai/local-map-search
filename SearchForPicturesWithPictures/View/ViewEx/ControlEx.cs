﻿namespace SearchForPicturesWithPictures.View.ViewEx
{
    internal static class ControlEx
    {
        /// <summary>
        /// 跨线程操作控件
        /// </summary>
        /// <param name="con"></param>
        /// <param name="action"></param>
        public static void ExecBeginInvoke(this Control con, Action action)
        {
            if (action == null) return;
            if (con.InvokeRequired)
            {
                con.BeginInvoke(new Action(action));
            }
            else
            {
                action();
            }
        }
    }
}
