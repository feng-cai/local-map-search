﻿using System.Drawing.Drawing2D;

namespace SearchForPicturesWithPictures.View.ViewEx
{
    /// <summary>
    /// 封装生成高级衍生图形的各种静态方法
    /// </summary>
    public static class ExtendedShapes
    {
        public static PointF[] CreateRegularPolygon(PointF pivot, float outterRadius, int points, float angleOffset)
        {
            if (outterRadius < 0) throw new ArgumentOutOfRangeException("outterRadius", "多边形的外接圆半径必须大于0");

            if (points < 3) throw new ArgumentOutOfRangeException("points", "多边形的边数必须大于等于3");
            PointF[] ret = new PointF[points];
            CircleF c = new CircleF(pivot, outterRadius); float ang = 360.0f / points;
            for (int i = 0; i < points; i++)
            {
                ret[i] = c.PointOnPath(angleOffset + i * ang);
            }
            return ret;
        }
        public static PointF[] CreateRegularPolygon(PointF pivot, float outterRadius, int points)
        {
            return CreateRegularPolygon(pivot, outterRadius, points, 0.0f);
        }
        public static GraphicsPath CreateStar(PointF pivot, float outterRadius, float innerRadius, int points, float angleOffset)
        {
            if (outterRadius <= innerRadius) throw new ArgumentException("参数 outterRadius必须大于innerRadius。");
            if (points < 2) throw new ArgumentOutOfRangeException("points"); GraphicsPath gp = new GraphicsPath();
            CircleF outter = new CircleF(pivot, outterRadius);
            CircleF inner = new CircleF(pivot, innerRadius); float ang = 360.0f / points;
            for (int i = 0; i < points; i++)
            {
                gp.AddLine(outter.PointOnPath(angleOffset + i * ang), inner.PointOnPath(angleOffset + i * ang + ang / 2.0f));
            }

            gp.CloseFigure();
            return gp;
        }

        public static GraphicsPath CreateStar(PointF pivot, float outterRadius, float innerRadius, int points)
        {
            return CreateStar(pivot, outterRadius, innerRadius, points, 0);
        }
        public static GraphicsPath CreateChamferBox(RectangleF rect, float radius)
        {
            GraphicsPath gp = new GraphicsPath();
            if (radius <= 0) { gp.AddRectangle(rect); return gp; }
            gp.AddArc(rect.Right - 2 * radius, rect.Top, radius * 2, radius * 2, 270, 90);
            gp.AddArc(rect.Right - radius * 2, rect.Bottom - radius * 2, radius * 2, radius * 2, 0, 90);
            gp.AddArc(rect.Left, rect.Bottom - 2 * radius, 2 * radius, 2 * radius, 90, 90);
            gp.AddArc(rect.Left, rect.Top, 2 * radius, 2 * radius, 180, 90); gp.CloseFigure();
            return gp;
        }
        public static List<PointF[]> CreateWaitCircleSpokes(PointF pivot, float outterRadius, float innerRadius, int spokes, float angleOffset)
        {
            if (spokes < 1) throw new ArgumentException("参数spokes必须大于等于。"); List<PointF[]> lst = new List<PointF[]>();
            CircleF outter = new CircleF(pivot, outterRadius); CircleF inner = new CircleF(pivot, innerRadius);
            float ang = 360.0f / spokes;
            for (int i = 0; i < spokes; i++)
                lst.Add(new PointF[2] { outter.PointOnPath(angleOffset + i * ang), inner.PointOnPath(angleOffset + i * ang) });
            return lst;
        }

        public static List<PointF[]> CreateWaitCircleSpokes(PointF pivot, float outterRadius, float innerRadius, int spokes)
        {
            return CreateWaitCircleSpokes(pivot, outterRadius, innerRadius, spokes,
            0.0f);
        }
    }
}
