﻿using System.ComponentModel;
using System.Runtime.InteropServices;

namespace SearchForPicturesWithPictures.View.ViewEx
{
    [Serializable, StructLayout(LayoutKind.Sequential), TypeConverter(typeof(CircleFConverter))]
    public sealed class CircleF : ICloneable
    {
        public static CircleF Empty = new CircleF();
        PointF _pivot;//圆心
        float _radius;//半径
        public CircleF() : this(default, 0)
        {

        }
        public CircleF(RectangleF rectf)
        {
            _pivot = GetCirclePivot(rectf);
            _radius = GetCircleRadius(rectf);
        }

        public CircleF(PointF pivot, float radius)
        {
            _pivot = pivot;
            _radius = radius;
        }
        public static PointF GetCirclePivot(RectangleF rect)
        {
            return new PointF(rect.X + rect.Width / 2.0f, rect.Y + rect.Height / 2.0f);
        }
        public static float GetCircleRadius(RectangleF rect)
        {
            return Math.Min(rect.Width / 2.0f, rect.Height / 2.0f);
        }
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public PointF Pivot { get { return _pivot; } set { _pivot = value; } }
        public float Radius
        {
            get { return _radius; }
            set
            {
                if (value >= 0)
                    _radius = value;
            }
        }
        public RectangleF GetBounds()
        {
            return new RectangleF(_pivot.X - _radius, _pivot.Y - _radius, _radius * 2.0f, _radius * 2.0f);
        }
        public static RectangleF GetBounds(RectangleF rect)
        {
            PointF p = GetCirclePivot(rect);
            float d = GetCircleRadius(rect);
            return new RectangleF(p.X - d, p.Y - d, d * 2.0f, d * 2.0f);
        }
        public bool Contains(float x, float y)
        {
            return Contains(new PointF(x, y));
        }

        public bool Contains(PointF p)
        {
            return Distance(p) <= _radius;
        }

        public float Distance(CircleF c)
        {
            return Distance(c.Pivot);
        }
        public float Distance(PointF p)
        {
            return (float)Math.Sqrt((p.X - _pivot.X) * (p.X - _pivot.X) + (p.Y - _pivot.Y) * (p.Y - _pivot.Y));
        }
        public object Clone()
        {
            return MemberwiseClone();
        }
        public PointF PointOnPath(double sweepAngle)
        {
            return new PointF(_pivot.X + _radius * (float)Math.Cos(sweepAngle * (Math.PI / 180.0d)), _pivot.Y - _radius * (float)Math.Sin(sweepAngle * (Math.PI / 180.0d)));
        }
        public void Offset(float x, float y)
        {
            _pivot.X += x;
            _pivot.Y += y;
        }
        public void Offset(PointF pos)
        {
            Offset(pos.X, pos.Y);
        }

        public void Inflate(float d)
        {
            Radius += d;
        }
        public static bool operator !=(CircleF left, CircleF right)
        {
            return left._pivot == right._pivot && left._radius == right._radius;
        }
        public static bool operator ==(CircleF left, CircleF right)
        {
            return !(left != right);
        }
        public override string ToString()
        {
            return string.Format("{{Pivot={0},Radius={1}}}", _pivot, _radius);
        }
        public static implicit operator RectangleF(CircleF c)
        {
            return c.GetBounds();
        }
    }
    public class CircleFConverter : TypeConverter
    {
        public CircleFConverter()
        {

        }
        public override bool CanConvertTo(ITypeDescriptorContext context, Type destinationType)
        {
            if (destinationType == typeof(string))
                return true;
            return base.CanConvertTo(context, destinationType);
        }

        public override object ConvertTo(ITypeDescriptorContext context, System.Globalization.CultureInfo culture, object value, Type destinationType)
        {
            if (value is CircleF)
            {
                if (destinationType == typeof(string))
                {
                    CircleF c = value as CircleF;
                    return string.Format("({0},{1}),{2}", c.Pivot.X, c.Pivot.Y, c.Radius);
                }
            }
            return base.ConvertTo(context, culture, value, destinationType);
        }

        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
        {
            if (sourceType == typeof(string))
                return true;
            else
                return base.CanConvertFrom(context, sourceType);
        }

        public override object ConvertFrom(ITypeDescriptorContext context, System.Globalization.CultureInfo culture, object value)
        {
            if (value is string)
            {
                string[] ti = ((string)value).Split(',');
                float x = float.Parse(ti[0].Substring(1));
                float y = float.Parse(ti[1].Substring(0, ti[1].Length - 1));
                float r = float.Parse(ti[2]);
                return new CircleF(new PointF(x, y), r);
            }
            return base.ConvertFrom(context, culture, value);
        }

        public override PropertyDescriptorCollection? GetProperties(ITypeDescriptorContext? context, object value, Attribute[]? attributes)
        {
            return TypeDescriptor.GetProperties(typeof(CircleF), attributes).Sort(new string[] { "Radius", "Pivot" });
        }
        public override bool GetPropertiesSupported(ITypeDescriptorContext? context)
        {
            return true;
        }
    }
}
