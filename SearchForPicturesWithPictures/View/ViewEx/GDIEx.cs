﻿namespace SearchForPicturesWithPictures.View.ViewEx
{
    internal static class GDIEx
    {
        /// <summary>
        /// 计算文字大小
        /// </summary>
        /// <param name="text">文字</param>
        /// <param name="font">字体</param>
        /// <returns>大小</returns>
        public static SizeF MeasureString(this string text, Font font)
        {
            return Graphics().MeasureString(text, font);
        }
        private static Graphics TempGraphics;

        /// <summary>
        /// 提供一个Graphics，常用于需要计算文字大小时
        /// </summary>
        /// <returns>大小</returns>
        public static Graphics Graphics()
        {
            if (TempGraphics == null)
            {
                Bitmap bmp = new Bitmap(1, 1);
                TempGraphics = bmp.Graphics();
            }

            return TempGraphics;
        }
        /// <summary>
        /// 图片的绘图图元
        /// </summary>
        /// <param name="image">图片</param>
        /// <returns>绘图图元</returns>
        public static Graphics Graphics(this Image image)
        {
            return System.Drawing.Graphics.FromImage(image);
        }
    }
}
